﻿using System;
using System.Collections.Generic;

namespace firstProblem
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 7024;
            var arr = new List<int>(number);
            for (int i = 0; i < number; i++)
            {
                arr.Add(i + 1);
            }

            Console.WriteLine("Enter number:");

            Console.WriteLine(Drop(arr));

        }

        public static int Drop(List<int> arr)
        {
            List<int> temp = new List<int>();
            if (arr.Count > 1)
            {
                if (arr.Count % 2 == 0)
                {
                    temp = new List<int>(arr.Count / 2);
                    for (int i = 0; i < arr.Count; i += 2)
                    {
                        temp.Add(arr[i]);
                    }
                }
                else
                {
                    temp = new List<int>(arr.Count / 2 + 1) { arr[arr.Count - 1] };
                    for (int i = 0; i < arr.Count - 1; i += 2)
                    {
                        temp.Add(arr[i]);
                    }
                }
                return Drop(temp);
            }


            return arr[0];
        }
    }
}
